package ru.farfor.ufa.farfor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimpleAdapterWithImageUrl extends SimpleAdapter {
    private Context mContext;
    public LayoutInflater inflater = null;

    public SimpleAdapterWithImageUrl(Context context,
                           List<? extends Map<String, ?>> data, int resource, String[] from,
                           int[] to) {
        super(context, data, resource, from, to);
        mContext = context;
        inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @SuppressWarnings("unchecked")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.item_offer, parent, false);
        HashMap<String, Object> data = (HashMap<String, Object>) getItem(position);

        String urlImage = (String) data.get("image");
        if (urlImage != null) {
            Picasso.with(mContext)
                    .load(urlImage)
                    .fit()
                    .centerInside()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into((ImageView) vi.findViewById(R.id.iv_img));
        }
        ((TextView)vi.findViewById(R.id.tv_name)).setText((String)data.get("name"));
        ((TextView)vi.findViewById(R.id.tv_weight)).setText((String)data.get("weight"));
        ((TextView)vi.findViewById(R.id.tv_price)).setText((String)data.get("price"));

        return vi;
    }

}