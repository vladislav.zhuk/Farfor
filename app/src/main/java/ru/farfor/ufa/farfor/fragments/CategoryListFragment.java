package ru.farfor.ufa.farfor.fragments;


import android.app.ListFragment;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.redmadrobot.chronos.ChronosConnector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.farfor.ufa.farfor.Deserializer;
import ru.farfor.ufa.farfor.DownloadXmlOperation;
import ru.farfor.ufa.farfor.MyApplication;
import ru.farfor.ufa.farfor.R;
import ru.farfor.ufa.farfor.model.Category;

public class CategoryListFragment extends ListFragment {

    private ChronosConnector mConnector = new ChronosConnector();

    private static final String ATTRIBUTE_NAME_TEXT = "text";
    private static final String ATTRIBUTE_NAME_IMAGE = "image";

    private Context mContext;

    List<Category> categoryList;

    public interface onCategorySelectedListener {
        void onCategorySelected(int categoryId);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mConnector.onCreate(this, savedInstanceState);
        mConnector.runOperation(new DownloadXmlOperation(((MyApplication)(getActivity().getApplication())).getXmlFile()), false);
    }

    @Override
    public void onResume() {
        super.onResume();
        mConnector.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mConnector.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        mConnector.onPause();
        super.onPause();
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        ((onCategorySelectedListener)mContext).onCategorySelected(categoryList.get(position).getId());
    }

    private SimpleAdapter createAdapter(List<Category> categoryList) {

        int amount = categoryList.size();

        List<Map<String, Object>> data = new ArrayList<>(amount);
        Map<String, Object> mapItem;
        for (Category category : categoryList) {
            mapItem = new HashMap<>();
            mapItem.put(ATTRIBUTE_NAME_TEXT, category.getName());
            mapItem.put(ATTRIBUTE_NAME_IMAGE, category.getImage());
            data.add(mapItem);
        }

        String[] from = {ATTRIBUTE_NAME_TEXT, ATTRIBUTE_NAME_IMAGE};
        int[] to = {R.id.tv_text, R.id.iv_img};

        return new SimpleAdapter(getActivity(), data, R.layout.item_category, from, to);
    }

    private List<Category> fillListCategories() {
        TypedArray categoriesDrawables = getResources().obtainTypedArray(R.array.categoriesDrawable);
        int[] ids = getResources().getIntArray(R.array.categoriesId);
        List<Category> categoryList = Deserializer.getInstance().initialize(((MyApplication)(getActivity().getApplication())).getXmlFile()).getCategoriesList();

        for(Category category : categoryList) {
            for(int i = 0; i < ids.length; i++) {
                if (ids[i] == category.getId()) {
                    category.setImage(categoriesDrawables.getResourceId(i, 0));
                }
            }
        }

        categoriesDrawables.recycle();

        return categoryList;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onOperationFinished(final DownloadXmlOperation.Result result) {
        if (result.isSuccessful()) {
            categoryList = fillListCategories();
            setListAdapter(createAdapter(categoryList));
        } else {
            Toast.makeText(mContext, result.getErrorMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
