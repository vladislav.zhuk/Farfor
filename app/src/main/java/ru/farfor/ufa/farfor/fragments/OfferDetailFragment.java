package ru.farfor.ufa.farfor.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import ru.farfor.ufa.farfor.Constants;
import ru.farfor.ufa.farfor.Deserializer;
import ru.farfor.ufa.farfor.R;
import ru.farfor.ufa.farfor.model.Offer;

public class OfferDetailFragment extends Fragment {

    private Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_offer_detail, container, false);

        Offer offer = Deserializer.getInstance().getOfferById(getArguments().getInt(Constants.OFFER_ID));

        Picasso.with(mContext)
                .load(offer.getPictureUrl())
                .fit()
                .centerInside()
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into((ImageView) view.findViewById(R.id.iv_img));
        ((TextView) view.findViewById(R.id.tv_weight)).setText(getString(R.string.weight, offer.getWeight()));
        ((TextView) view.findViewById(R.id.tv_price)).setText(getString(R.string.price, offer.getPrice()));
        ((TextView) view.findViewById(R.id.tv_description)).setText(offer.getDescription());
        return view;
    }
}
