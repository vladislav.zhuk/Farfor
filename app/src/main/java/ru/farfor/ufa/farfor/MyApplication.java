package ru.farfor.ufa.farfor;

import android.app.Application;

import java.io.File;

public class MyApplication extends Application {
    private File xmlFile;

    public File getXmlFile() {
        if(xmlFile == null) {
            xmlFile = new File(getApplicationInfo().dataDir, "xml");
        }
        return xmlFile;
    }
}
