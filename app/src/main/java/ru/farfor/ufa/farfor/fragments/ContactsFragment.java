package ru.farfor.ufa.farfor.fragments;


import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import ru.farfor.ufa.farfor.R;

public class ContactsFragment extends MapFragment implements OnMapReadyCallback {

    GoogleMap map;

    public static final int LOCATION_REQUEST_CODE = 1;
    private LatLng ufa;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        FrameLayout mapContainer = (FrameLayout) rootView.findViewById(R.id.map_container);
        View mapView = super.onCreateView(inflater, mapContainer, savedInstanceState);
        mapContainer.addView(mapView,
                new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        getMapAsync(this);
        return rootView;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;

        ufa = new LatLng(54.753348, 56.014672);
        map.addMarker(new MarkerOptions().position(ufa).title("Farfor"));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(ufa, 15.0f));

        if (!checkPermission()) {
            requestPermissions(new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
            }, LOCATION_REQUEST_CODE);
        } else {
            setMyLocationEnabled();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        setMyLocationEnabled();
    }

    private boolean checkPermission() {
        return ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void setMyLocationEnabled() {
        if(checkPermission()) {
            map.setMyLocationEnabled(true);
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(ufa, 3.0f));
        } else {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(ufa, 15.0f));
        }
    }
}
