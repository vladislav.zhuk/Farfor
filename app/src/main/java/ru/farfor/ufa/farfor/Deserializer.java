package ru.farfor.ufa.farfor;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ru.farfor.ufa.farfor.model.Categories;
import ru.farfor.ufa.farfor.model.Category;
import ru.farfor.ufa.farfor.model.Offer;
import ru.farfor.ufa.farfor.model.Offers;

public class Deserializer {

    private File mXmlFile;
    private List<Offer> offersList;
    private List<Category> categoriesList;
    private static Deserializer instance = new Deserializer();

    private Deserializer() {
    }

    public static Deserializer getInstance() {
        return instance;
    }

    public Deserializer initialize(File xmlFile) {
        mXmlFile = xmlFile;
        return instance;
    }

    public List<Category> getCategoriesList() {
        if(categoriesList == null) {
            Categories categories = deserialize(Categories.class);
            if (categories != null) {
                categoriesList = categories.getCategoriesList();
            }
        }
        return categoriesList;
    }

    public List<Offer> getOffersListByCategoryId(int categoryId) {
        if(offersList == null) {
            Offers offers = deserialize(Offers.class);
            if (offers != null) {
                offersList = offers.getOffersList();
            }
        }
        List<Offer> result = new ArrayList<>();
        for(Offer offer : offersList) {
            if(offer.getCategoryId() == categoryId) {
                result.add(offer);
            }
        }
        return result;
    }

    public Offer getOfferById(int id) {
        for(Offer offer : offersList) {
            if(offer.getId() == id) {
                return offer;
            }
        }
        return null;
    }

    private <T> T deserialize(Class<? extends T> value) {
        Serializer serializer = new Persister();
        try {
            return serializer.read(value, mXmlFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
