package ru.farfor.ufa.farfor;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.redmadrobot.chronos.ChronosOperation;
import com.redmadrobot.chronos.ChronosOperationResult;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadXmlOperation extends ChronosOperation<Void> {

    private final File xmlFile;

    public DownloadXmlOperation(File xmlFile) {
        this.xmlFile = xmlFile;
    }

    @Nullable
    @Override
    public Void run() {
        try {
            URL url = new URL("http://ufa.farfor.ru/getyml?key=ukAXxeJYZN");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();

            FileOutputStream fos = new FileOutputStream(xmlFile);

            byte[] buffer = new byte[1024];
            int bufferLength;

            while ((bufferLength = inputStream.read(buffer)) > 0) {
                fos.write(buffer, 0, bufferLength);
            }
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @NonNull
    @Override
    public Class<? extends ChronosOperationResult<Void>> getResultClass() {
        return Result.class;
    }

    public final static class Result extends ChronosOperationResult<Void> {
    }
}
