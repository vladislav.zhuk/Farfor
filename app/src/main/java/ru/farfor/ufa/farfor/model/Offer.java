package ru.farfor.ufa.farfor.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name="offer", strict = false)
public class Offer {

    @Attribute(name="id")
    private int id;

    @Element
    private String name;

    @Element
    private String price;

    @Element(required = false)
    private String description;

    @Element(name = "picture", required = false)
    private String pictureUrl;

    @Element
    private int categoryId;

    @ElementList(inline = true, required = false)
    private List<Param> paramList;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public List<Param> getParamList() {
        return paramList;
    }

    public String getWeight(){
        if (getParamList() != null) {
            for(Param p : paramList) {
                if(p.getName().equals("Вес")) {
                    return p.getValue();
                }
            }
        }
        return "";
    }
}
