package ru.farfor.ufa.farfor.fragments;

import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.farfor.ufa.farfor.Constants;
import ru.farfor.ufa.farfor.Deserializer;
import ru.farfor.ufa.farfor.R;
import ru.farfor.ufa.farfor.SimpleAdapterWithImageUrl;
import ru.farfor.ufa.farfor.model.Offer;

public class OfferListFragment extends ListFragment {

    private static final String ATTRIBUTE_NAME_NAME = "name";
    private static final String ATTRIBUTE_NAME_IMAGE = "image";
    private static final String ATTRIBUTE_NAME_WEIGHT = "weight";
    private static final String ATTRIBUTE_NAME_PRICE = "price";

    private List<Offer> offersList;
    private Context mContext;

    public interface onOfferSelectedListener {
        void onOfferSelected(int offerId);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        offersList = Deserializer.getInstance().getOffersListByCategoryId(getArguments().getInt(Constants.CATEGORY_ID));

        setListAdapter(createAdapter());
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        ((onOfferSelectedListener)mContext).onOfferSelected(offersList.get(position).getId());
    }

    private SimpleAdapterWithImageUrl createAdapter() {

        List<Map<String, Object>> data = new ArrayList<>(offersList.size());
        Map<String, Object> mapItem;
        for (Offer offer : offersList) {
            mapItem = new HashMap<>();
            mapItem.put(ATTRIBUTE_NAME_IMAGE, offer.getPictureUrl());
            mapItem.put(ATTRIBUTE_NAME_NAME, offer.getName());
            mapItem.put(ATTRIBUTE_NAME_WEIGHT, offer.getWeight());
            mapItem.put(ATTRIBUTE_NAME_PRICE, offer.getPrice());
            data.add(mapItem);
        }

        String[] from = {ATTRIBUTE_NAME_IMAGE, ATTRIBUTE_NAME_NAME, ATTRIBUTE_NAME_WEIGHT, ATTRIBUTE_NAME_PRICE};
        int[] to = {R.id.iv_img, R.id.tv_name, R.id.tv_weight, R.id.tv_price};

        return new SimpleAdapterWithImageUrl(mContext, data, R.layout.item_offer, from, to);
    }
}
