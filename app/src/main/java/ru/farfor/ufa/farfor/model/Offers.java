package ru.farfor.ufa.farfor.model;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "offers", strict = false)
public class Offers {
    @ElementList(inline=true)
    @Path("shop/offers")
    private List<Offer> offersList;

    public List<Offer> getOffersList() {
        return offersList;
    }
}
