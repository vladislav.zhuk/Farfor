package ru.farfor.ufa.farfor;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import ru.farfor.ufa.farfor.fragments.CategoryListFragment;
import ru.farfor.ufa.farfor.fragments.CategoryListFragment.onCategorySelectedListener;
import ru.farfor.ufa.farfor.fragments.ContactsFragment;
import ru.farfor.ufa.farfor.fragments.OfferDetailFragment;
import ru.farfor.ufa.farfor.fragments.OfferListFragment;
import ru.farfor.ufa.farfor.fragments.OfferListFragment.onOfferSelectedListener;

import static android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;

public class MainActivity extends AppCompatActivity
        implements OnNavigationItemSelectedListener, onCategorySelectedListener, onOfferSelectedListener {

    private NavigationView navigationView;
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (getFragmentManager().findFragmentById(R.id.fragment) == null) {
            openFragment(new CategoryListFragment(), false);
            navigationView.setCheckedItem(R.id.nav_catalog);
            changeTitle("Каталог");
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
            navigationView.setCheckedItem(R.id.nav_catalog);
            changeTitle("Каталог");
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_catalog:
                FragmentManager fm = getFragmentManager();
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                break;
            case R.id.nav_contacts:
                openFragment(new ContactsFragment(), true);
                changeTitle("Контакты");
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onCategorySelected(int categoryId) {
        Bundle args = new Bundle();
        args.putInt(Constants.CATEGORY_ID, categoryId);
        Fragment offerListFragment = new OfferListFragment();
        offerListFragment.setArguments(args);
        openFragment(offerListFragment, true);
    }

    @Override
    public void onOfferSelected(int offerId) {
        Bundle args = new Bundle();
        args.putInt(Constants.OFFER_ID, offerId);
        Fragment offerDetailFragment = new OfferDetailFragment();
        offerDetailFragment.setArguments(args);
        openFragment(offerDetailFragment, true);
    }

    private void openFragment(Fragment fragment, Boolean addToBackStack) {
        if (fragment != null) {
            fragment.setRetainInstance(true);
            FragmentTransaction fTrans = getFragmentManager().beginTransaction();
            fTrans.replace(R.id.fragment, fragment);
            if (addToBackStack) {
                fTrans.addToBackStack(null);
            }
            fTrans.commit();
        }
    }

    private void changeTitle(String s) {
        if (actionBar != null) {
            actionBar.setTitle(s);
        }
    }
}
