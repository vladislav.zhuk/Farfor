package ru.farfor.ufa.farfor.model;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "categories", strict = false)
public class Categories {
    @ElementList(inline = true)
    @Path("shop/categories")
    private List<Category> categoriesList;

    public List<Category> getCategoriesList() {
        return categoriesList;
    }
}
