package ru.farfor.ufa.farfor.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name = "param")
public class Param {

    @Attribute
    private String name;

    @Text
    private String value;

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }
}
